/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;


import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

/**
 *
 * @author Vitas
 */
public class Menu extends javax.swing.JFrame implements Runnable{

    Login logout = new Login();
    static String iloscG = null;
    Thread timerr;
    
    /**
     * Konstruktor umożliwiający domyślne wylogowanie z serwera oraz zapisania akcji(logów) do bazy danych.
     */
    public Menu() {
        initComponents();
        myInitComponents();
    }

    private void myInitComponents(){
        addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosing(WindowEvent e){
                action("Wyjscie");
                logoutt();
                e.getWindow().dispose();
            }
        });   
        setLocationRelativeTo(null);
        start();
        pack();    
   }
   
    
    public void start(){
        if(timerr == null) timerr = new Thread(this);
        timerr.start();
    }
    /**
     * Metoda odpowiadająca za wysyłanie na serwer informacji o graczu, którego ma wylogować
     */    
    public void logoutt(){
        try{
            BufferedReader in;
            PrintWriter out;
            Socket s = new Socket( cServer.url, 9090);
            in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            out = new PrintWriter(s.getOutputStream(), true);

            String login_request = "logout:" + logout.login() + ":" + logout.passwd;

                System.out.println(login_request);
                out.println(login_request);
        } catch (IOException ex) {
            Login.saveError(ex);
        }
    
    }

    /**
     * Metoda odpowiadająca za wysyłanie na serwer informacji o zapisaniu logów do bazy danych
     * @param akcja
     */       
    public void action(String akcja){
        try{
            BufferedReader in;
            PrintWriter out;
            Socket s = new Socket( cServer.url, 9090);
            in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            out = new PrintWriter(s.getOutputStream(), true);

            String login_request = "akcja:" + logout.login() + ":" + Login.ID + ":" + akcja;

                System.out.println(login_request);
                out.println(login_request);
        } catch (IOException ex) {
            Login.saveError(ex);
        }
    
    }
 
     /**
     * Metoda odpowiadająca za wysyłanie na serwer informacji o wyjściu gracza.
     */ 
    public void outgame(){
        try{
            BufferedReader in;
            PrintWriter out;
            Socket s = new Socket( cServer.url, 9090);
            in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            out = new PrintWriter(s.getOutputStream(), true);

            String login_request = "outgame:" + logout.login() + ":" + logout.passwd + ":" + Player.getID();

                System.out.println(login_request);
                out.println(login_request);
        } catch (IOException ex) {
            Login.saveError(ex);
        }
    }

     /**
     * Metoda odpowiadająca sprawdzenie ile graczy jest aktualnie na serverze
     * @return 
     */     
    public String checkPlayer(){
        try{
            BufferedReader in;
            PrintWriter out;
            Socket s = new Socket( cServer.url, 9090);
            in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            out = new PrintWriter(s.getOutputStream(), true);

            String login_request = "checkplayer:" + logout.login() + ":" + logout.passwd;

                System.out.println(login_request);
                out.println(login_request);
                String response;
                response = in.readLine();
                return response;
        } catch (IOException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
            return "Blad w przeszukiwaniu graczy";
        }        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        newGameButton = new javax.swing.JLabel();
        logoLabel = new javax.swing.JLabel();
        infoButton = new javax.swing.JLabel();
        exitButton = new javax.swing.JLabel();
        rankingButton = new javax.swing.JLabel();
        logoutButton = new javax.swing.JLabel();
        Zegarek = new javax.swing.JTextField();
        backgroundMenu = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(800, 600));
        setResizable(false);
        getContentPane().setLayout(null);

        newGameButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/game/pictures/newGameButton.png"))); // NOI18N
        newGameButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                newGameButtonMousePressed(evt);
            }
        });
        getContentPane().add(newGameButton);
        newGameButton.setBounds(350, 210, 126, 60);

        logoLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/game/pictures/logo.png"))); // NOI18N
        getContentPane().add(logoLabel);
        logoLabel.setBounds(290, 30, 224, 98);

        infoButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/game/pictures/infoButton.png"))); // NOI18N
        infoButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                infoButtonMousePressed(evt);
            }
        });
        getContentPane().add(infoButton);
        infoButton.setBounds(350, 290, 126, 50);

        exitButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/game/pictures/exitButton.jpg"))); // NOI18N
        exitButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                exitButtonMousePressed(evt);
            }
        });
        getContentPane().add(exitButton);
        exitButton.setBounds(350, 450, 126, 54);

        rankingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/game/pictures/rankingButton.png"))); // NOI18N
        rankingButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                rankingButtonMousePressed(evt);
            }
        });
        getContentPane().add(rankingButton);
        rankingButton.setBounds(350, 370, 126, 54);

        logoutButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/game/pictures/logoutButton.png"))); // NOI18N
        logoutButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                logoutButtonMousePressed(evt);
            }
        });
        getContentPane().add(logoutButton);
        logoutButton.setBounds(10, 520, 95, 40);

        Zegarek.setEditable(false);
        getContentPane().add(Zegarek);
        Zegarek.setBounds(700, 550, 90, 20);

        backgroundMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/game/pictures/tlo.jpg"))); // NOI18N
        getContentPane().add(backgroundMenu);
        backgroundMenu.setBounds(0, 0, 800, 600);

        pack();
    }// </editor-fold>//GEN-END:initComponents
   
    private void exitButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exitButtonMousePressed
        action("Wyjscie");
        logoutt();
        System.exit(0);
    }//GEN-LAST:event_exitButtonMousePressed

    private void rankingButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rankingButtonMousePressed
        action("Ranking");
        rankingButton.setToolTipText("Kliknij aby przejsc do rankingu");
        Ranking records = new Ranking();
        records.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        records.setVisible(true);
        dispose();
    }//GEN-LAST:event_rankingButtonMousePressed

    private void infoButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_infoButtonMousePressed
        action("Informacje");
        Info rank = new Info();
        rank.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        rank.setVisible(true);
        dispose();
    }//GEN-LAST:event_infoButtonMousePressed

    private void logoutButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoutButtonMousePressed
        action("Wylogowanie");
        logoutt();
        logout.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        logout.setVisible(true);
        dispose();
    }//GEN-LAST:event_logoutButtonMousePressed

    private void newGameButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_newGameButtonMousePressed
        try{
            BufferedReader in;
            PrintWriter out;
            Socket s = new Socket( cServer.url, 9090);
            in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            out = new PrintWriter(s.getOutputStream(), true);

            String login_request = "ingame:" + logout.login() + ":" + logout.passwd;

                System.out.println(login_request);
                out.println(login_request);
        } catch (IOException ex) {
            Login.saveError(ex);
        }
        iloscG = checkPlayer();
        System.out.println("MENU PO KLIKNIECIU : "+ iloscG);
        action("NowaGra");
        Play gra = new Play();
        gra.setVisible(true);
        dispose();
    }//GEN-LAST:event_newGameButtonMousePressed



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField Zegarek;
    private javax.swing.JLabel backgroundMenu;
    private javax.swing.JLabel exitButton;
    private javax.swing.JLabel infoButton;
    private javax.swing.JLabel logoLabel;
    private javax.swing.JLabel logoutButton;
    private javax.swing.JLabel newGameButton;
    private javax.swing.JLabel rankingButton;
    // End of variables declaration//GEN-END:variables
 
    @Override
    public void run() {
        while (timerr == Thread.currentThread() ){
            try {
                Zegarek.setText(cServer.ee.timeNow());
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException ex) {
                Login.saveError(ex);
            }
        }
    }

}

